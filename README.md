Implement MyList.ts by implementing List.ts
but cannot use Collection frameworks.  
On each case, all the test cases should be passed.

List.tsを実装したMyList.tsを作成せなさい、ただし、コレクションフレームワークを使わないこと。  
いずれの場合も用意されているテストクラスをすべて合格すること。

Cài đặt MyList.ts bằng cách cài đặt lại List.ts
nhưng không sử dụng framework Collection đã có.  
Trong từng trường hợp, tất cả các bài kiểm tra nên được qua.

テストに誤りを発見した場合、おしらせください。  
When you find some suspicous tests, inform us.  
Trong trường hợp bạn tìm ra lỗi trong các bài kiểm tra, thông báo cho chúng tôi.

### Execute tests

1. install jest, ts-jest
2. type `yarn test` and hit the return key

### Answer file

Answer file is zipped in this repository. `answer.zip`
