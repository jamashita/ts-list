export class NoSuchElementException extends Error {

  public constructor(message: string) {
    super(message);
  }
}
