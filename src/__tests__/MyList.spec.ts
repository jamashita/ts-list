import { MyList } from '../answer/MyList';
import { List } from '../List';
import { NoSuchElementException } from '../NoSuchElementException';

describe('MyList', () => {
  it('addGetSetInsertShiftTest', () => {
    const list: List<unknown> = new MyList<unknown>();
    expect(list.size()).toEqual(0);

    expect(() => {
      list.get(-1);
    }).toThrow(NoSuchElementException);
    expect(() => {
      list.set('a', -1);
    }).toThrow(NoSuchElementException);
    expect(() => {
      list.insert('b', -1);
    }).toThrow(NoSuchElementException);
    expect(() => {
      list.get(0);
    }).toThrow(NoSuchElementException);
    expect(() => {
      list.set(null, 0);
    }).toThrow(NoSuchElementException);
    expect(() => {
      list.get(1);
    }).toThrow(NoSuchElementException);
    expect(() => {
      list.set(null, 1);
    }).toThrow(NoSuchElementException);
    expect(list.contains(null)).toEqual(false);

    list.add(null);

    expect(list.size()).toEqual(1);
    expect(list.get(0)).toEqual(null);
    expect(() => {
      list.get(1);
    }).toThrow(NoSuchElementException);
    expect(() => {
      list.set('a', 1);
    }).toThrow(NoSuchElementException);
    expect(list.contains(null)).toEqual(true);

    list.add('a');

    expect(list.size()).toEqual(2);
    expect(list.contains('a')).toEqual(true);

    list.shift(null);

    expect(list.size()).toEqual(3);
    expect(list.get(0)).toEqual(null);
    expect(list.get(1)).toEqual(null);
    expect(list.get(2)).toEqual('a');

    list.set('b', 1);

    expect(list.size()).toEqual(3);
    expect(list.contains('b')).toEqual(true);
    expect(list.get(0)).toEqual(null);
    expect(list.get(1)).toEqual('b');
    expect(list.get(2)).toEqual('a');

    list.set(null, 2);

    expect(list.size()).toEqual(3);
    expect(list.get(0)).toEqual(null);
    expect(list.get(1)).toEqual('b');
    expect(list.get(2)).toEqual(null);

    list.insert('c', 0);
    list.insert('e', 3);
    list.insert(null, 2);

    expect(list.size()).toEqual(6);
    expect(list.contains('c')).toEqual(true);
    expect(list.contains('e')).toEqual(true);

    expect(list.get(0)).toEqual('c');
    expect(list.get(1)).toEqual(null);
    expect(list.get(2)).toEqual(null);
    expect(list.get(3)).toEqual('b');
    expect(list.get(4)).toEqual('e');
    expect(list.get(5)).toEqual(null);

    list.clear();
    expect(list.size()).toEqual(0);
    list.insert('g', 0);
    expect(list.size()).toEqual(1);
    expect(() => {
      list.get(1);
    }).toThrow(NoSuchElementException);
    expect(() => {
      list.set('a', 1);
    }).toThrow(NoSuchElementException);
    expect(() => {
      list.insert('b', 2);
    }).toThrow(NoSuchElementException);
  });

  it('removeDropTest', () => {
    const list: List<unknown> = new MyList<unknown>();
    list.drop();
    list.add('f');
    list.add(null);
    list.add('a');
    list.add(null);
    list.add('b');
    list.add('c');
    list.add('d');
    list.add('e');
    expect(() => {
      list.removeAt(-1);
    }).toThrow(NoSuchElementException);
    expect(list.size()).toEqual(8);
    expect(list.contains('f')).toEqual(true);
    list.drop();
    expect(list.size()).toEqual(7);
    expect(list.contains('f')).toEqual(false);
    expect(() => {
      list.removeAt(8);
    }).toThrow(NoSuchElementException);
    expect(list.contains('e')).toEqual(true);
    list.removeAt(6);
    expect(list.size()).toEqual(6);
    expect(list.contains('e')).toEqual(false);
    expect(list.contains(null)).toEqual(true);
    list.remove(null);
    expect(list.size()).toEqual(4);
    expect(list.contains(null)).toEqual(false);
  });

  it('subListTest', () => {
    const list: List<unknown> = new MyList<unknown>();
    list.add('a');
    list.add('b');
    list.add('c');
    list.add('d');
    list.add('e');
    list.add('f');
    const subList: List<unknown> = list.subList(3, 5);
    expect(subList.size()).toEqual(3);
    list.add('g');
    expect(list.size()).toEqual(7);
    expect(subList.size()).toEqual(3);
    list.clear();
    expect(subList.size()).toEqual(3);
  });

  it('joinTest', () => {
    const list1: List<string> = new MyList<string>();
    list1.add('a');
    list1.add('b');
    list1.add('c');
    const list2: List<string> = new MyList<string>();
    list2.add('d');
    list2.add('e');
    list2.add('f');
    expect(list1.size()).toEqual(3);
    list1.join(list2);
    expect(list1.size()).toEqual(6);
    list2.add('g');
    expect(list1.size()).toEqual(6);
    expect(list2.size()).toEqual(4);
    list2.clear();
    expect(list1.size()).toEqual(6);
  });
});
