export interface List<T> {

  /**
   * Get index-th element.
   * when fails, this method throws NoSuchElementException.
   * index番目の要素を取得する
   * 存在しないindexを指定された場合NoSuchElementExceptionを投げる
   * Lấy ra phần tử tại vị trí thứ index.
   * Nếu thất bại, tung ra NoSuchElementException.
   *
   * @param index {number}
   * @return {T}
   * @throws NoSuchElementException
   */
  get(index: number): T;

  /**
   * add t into this List's tail.
   * tをリストの末尾に追加する
   * thêm t vào đuôi của danh sách này.
   *
   * @param t {T}
   */
  add(t: T): void;

  /**
   * add t into this List's head.
   * tをリストの先端に追加する
   * thêm t vào đầu của danh sách này.
   *
   * @param t {T}
   */
  shift(t: T): void;

  /**
   * replace index-th element to t.
   * when fails, this method throws NoSuchElementException.
   * index番目をtにする
   * 存在しないindexを指定された場合NoSuchElementExceptionを投げる
   * thay thế phần tử thứ index bằng t
   * nếu thất bại, phương thức này tung ra NoSuchElementException.
   * [1, 2, 3]
   * do set(4, 1), you will get the following List.
   * nêú thực hiện set(4, 1), bạn sẽ nhận được danh sách dưới đây
   * [1, 4, 3]
   *
   * @param t {T}
   * @param index {number}
   * @throws NoSuchElementException
   */
  set(t: T, index: number): void;

  /**
   * insert t into offset-th.
   * when fails, this method throws NoSuchElementException.
   * offset番目にtを挿入する
   * 存在しないindexを指定された場合NoSuchElementExceptionを投げる
   * thêm phần tử t vào vị trí thứ offset
   * nếu thất bại, phương thức này tung ra NoSuchElementException.
   * [1, 2, 3]
   * do insert(4, 1), you will get the following List.
   * khi thực hiện insert(4, 1), bạn sẽ nhận được danh sách dưới đây.
   * [1, 4, 2, 3]
   *
   * @param t {T}
   * @param offset {number}
   * @throws NoSuchElementException
   */
  insert(t: T, offset: number): void;

  /**
   * when contains t in this List, returns true.
   * tが含まれているかを判定する
   * khi danh sách này chứa phần tử t, trả về true.
   *
   * @param t {T}
   * @return {boolean}
   */
  contains(t: T): boolean;

  /**
   * returns the size.
   * 長さを返す
   * trả về kích cỡ.
   *
   * @return {number}
   */
  size(): number;

  /**
   * remove all t from this List.
   * when there's no t in this List, do nothing.
   * tと同じものを全てリストから削除する
   * 一つもtと同じものがない場合、何もしない
   * loại bỏ toàn bộ phần tử t trong danh sách này.
   * khi không còn t trong danh sách này, không làm gì cả.
   *
   * @param t {T}
   */
  remove(t: T): void;

  /**
   * remove index-th element.
   * when fails, this method throws NoSuchElementException.
   * index番目を削除する
   * 存在しないindexを指定された場合NoSuchElementExceptionを投げる
   * loại bỏ phần tử thứ index.
   * khi thất bại, phương thức này tung NoSuchElementException.
   *
   * @param index {number}
   * @throws NoSuchElementException
   */
  removeAt(index: number): void;

  /**
   * remove the tail of this List.
   * when the List contains nothing, do nothing.
   * 末尾を削除する
   * 何もない場合、何もしない
   * loại bỏ phần tử đuôi của danh sách này.
   * khi danh sách không chứa gì, không làm gì cả.
   */
  pop(): void;

  /**
   * remove the head of this List.
   * when the List contains nothing, do nothing.
   * 先端を削除する
   * 何もない場合、何もしない
   * loại bỏ phần tử đầu của danh sách này.
   * khi danh sách không chứa gì, không làm gì cả.
   */
  drop(): void;

  /**
   * remove all elements.
   * すべての要素を削除する
   * loại bỏ tất cả các phần tử.
   */
  clear(): void;

  /**
   * extract from from-th to to-th into new List<T>.
   * extracted one shouldn't refers the same List.
   * from番目から、to番目までを新しくList<T>として抽出する
   * 抽出したリストは元のリストと同じものを参照しないこと
   * đưa các phần từ thứ thứ from tới thứ to vào danh sách mới List<T>
   * các phần tử của danh sách được tách ra không tham chiếu tới danh sách cũ
   *
   *
   * @param from {number}
   * @param to {number}
   * @return {List<T>}
   */
  subList(from: number, to: number): List<T>;

  /**
   * join other List into this List.
   * joined one shouldn't refers the same List.
   * リストを結合する
   * 結合したリストは元のリストと同じものを参照しないこと
   * gộp một danh sách khác vào danh sách này
   * danh sách đã được gộp không nên tham chiếu tới các danh sách thành phần
   *
   * @param other {List<T>}
   */
  join(other: List<T>): void;

  /**
   * when the List equals, returns true.
   * 同じリストであるかを判定する
   * khi 2 danh sách bằng nhau, trả về true.
   *
   * @param other {List<T>}
   * @return {boolean}
   */
  equals(other: List<T>): boolean;
}
